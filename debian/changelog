ocaml-doc (4.05-2) unstable; urgency=medium

  * Use /usr/share/dpkg/pkg-info.mk instead of manually calling and
    parsing dpkg-parsechangelog's output. This is possible since dpkg
    >= 1.16.1.
  * Bump dh compat to 10
  * Use a secure URL in debian/watch file
  * Convert d/copyright file to DEP-5 format
  * Bump Standards-Version to 4.1.3, no changes required.

 -- Mehdi Dogguy <mehdi@debian.org>  Sat, 06 Jan 2018 23:08:15 +0100

ocaml-doc (4.05-1) unstable; urgency=medium

  * Upload to Sid (Closes: #885865)

 -- Mehdi Dogguy <mehdi@debian.org>  Thu, 04 Jan 2018 21:45:49 +0100

ocaml-doc (4.05-1~exp1) experimental; urgency=medium

  * Import new upstream release.
  * Update Vcs-* fields
  * Bump DH compat to 9, no changes required

 -- Mehdi Dogguy <mehdi@debian.org>  Wed, 09 Aug 2017 16:56:54 -0400

ocaml-doc (4.02-2) unstable; urgency=medium

  * Delete a generated file (Closes: #803887).
  * Fix package description to mention only OCaml and not refer to
    "Objective Caml" anymore.

 -- Mehdi Dogguy <mehdi@debian.org>  Sun, 03 Jan 2016 12:25:22 +0100

ocaml-doc (4.02-1) unstable; urgency=medium

  * Import new upstream release.
  * No need to include ocamlbuild-user-guide.pdf anymore since it is
    included in OCaml's manual.

 -- Mehdi Dogguy <mehdi@debian.org>  Sat, 10 Oct 2015 18:39:32 +0200

ocaml-doc (4.01-2) unstable; urgency=low

  [ Stéphane Glondu ]
  * Team upload
  * Update Vcs-*

  [ Ralf Treinen ]
  * dh_installdocs: Do not install *.in files (closes: #737617)

 -- Stéphane Glondu <glondu@debian.org>  Mon, 12 May 2014 13:33:10 +0200

ocaml-doc (4.01-1) unstable; urgency=low

  * Import 4.01 version.
    + OCamlBuild manual is now integrated into upstream's documentation
      (Closes: #464873).
    + ocaml.info.gz has now an info-dir-section statement (Closes: #528887).
  * Minor fixes in debian/generate_tarball:
    + Upstream now provides "ocaml-${ver}-refman-html.tar.gz" instead of
      "ocaml-${ver}-refman.html.tar.gz".
    + Cosmetic fix: don't use 3.12 in temporary directory names.

 -- Mehdi Dogguy <mehdi@debian.org>  Thu, 12 Dec 2013 21:07:58 +0100

ocaml-doc (3.12-2) unstable; urgency=low

  * Replace "3.11" with "@VERSION@" in debian/ocaml-doc.doc-base.ocaml.in

 -- Mehdi Dogguy <mehdi@debian.org>  Wed, 04 Jan 2012 16:49:06 +0100

ocaml-doc (3.12-1) unstable; urgency=low

  * New upstream release (Closes: #627791)
  * Add myself to Uploaders.
  * Add a (very ugly) script "generate_tarball" to generate new tarballs
    for ocaml-doc.
  * Clean debian/rules file by using dh7 rules style.
  * Bump Standards-Version to 3.9.2, no changes needed.
  * Add ${misc:Depends} in ocaml-doc's Depends field.
  * Add a Homepage field.
  * Fix install-info-used-in-maintainer-script by simply removing postinst
    and prerm scripts. Calls to install-info is now handled by triggers.
  * Add a watch file
  * Convert source package to 3.0 (quilt) format

 -- Mehdi Dogguy <mehdi@debian.org>  Wed, 04 Jan 2012 15:11:31 +0100

ocaml-doc (3.11-1) unstable; urgency=low

  [ Stefano Zacchiroli ]
  * fix vcs-svn field to point just above the debian/ dir

  [ Ralf Treinen ]
  * Added myself to Uploaders.
  * doc-base file:
    - changed section to Programming/OCaml.
    - fixed minor spelling errors.
    - converted to utf8.

  [ Samuel Mimram ]
  * Refresh documentation for OCaml 3.11.
  * Switch packaging to git
  * Update standards verstion to 3.8.0.
  * Update compat to 7.

 -- Samuel Mimram <smimram@debian.org>  Tue, 24 Feb 2009 20:46:08 +0100

ocaml-doc (3.10-1) unstable; urgency=low

  * New upstream release.
  * Removed camlp4 documentation since it is not up-to-date.
  * Updated to standards version 3.7.2, no changes needed.
  * Updated my email address.

 -- Samuel Mimram <smimram@debian.org>  Sat, 08 Sep 2007 01:49:22 +0200

ocaml-doc (3.09-1) unstable; urgency=low

  * New upstream release but no updated documentation for ocamlp4 yet
  * Make ocaml-team as Maintainer

 -- Remi Vanicat <vanicat@debian.org>  Sun, 27 Nov 2005 14:55:36 +0100

ocaml-doc (3.08.0-1) unstable; urgency=low

  * NMU Acknowledgment (thanks to Samuel).
  * Change the HTML registered index for the example
    (closes: #267421, #285661).
  * Added ocaml-team and Samuel Mimram has uploaders of this package
    (many thanks to him for his	work).

 -- Remi Vanicat <vanicat@debian.org>  Wed, 15 Dec 2004 16:15:02 +0100

ocaml-doc (3.08.0-0.3) unstable; urgency=low

  * NMU.
  * The some dirs seem to remain from the old package I don't know why, let's
    remove them.

 -- Samuel Mimram <samuel.mimram@ens-lyon.org>  Mon, 23 Aug 2004 23:26:43 +0200

ocaml-doc (3.08.0-0.2) unstable; urgency=low

  * NMU.
  * Removing the old /usr/share/doc/ocaml/docs directory if it exists and is
    empty to have a smoother upgrade.

 -- Samuel Mimram <samuel.mimram@ens-lyon.org>  Mon, 23 Aug 2004 13:25:12 +0200

ocaml-doc (3.08.0-0.1) unstable; urgency=medium

  * NMU.
  * Using Enhanced OCaml Documentation.
  * Building the package in binary-indep instead of binary-arch rule of the
    rules.
  * Fixed some typo in the copyright and added a link to Hendrik Tews' enhanced
    documentation.
  * Moved documents to /usr/share/doc/ocaml-doc with a symbolic link from
    /usr/share/doc/ocaml.

 -- Samuel Mimram <samuel.mimram@ens-lyon.org>  Fri, 13 Aug 2004 13:04:36 +0200

ocaml-doc (3.08-0.1) unstable; urgency=medium

  * NMU.
  * New upstream release but no updated documentation for ocamlp4 yet,
    closes: #262049.
  * Updated Standards-Version to 3.6.1.
  * Changelog converted to utf8.

 -- Samuel Mimram <samuel.mimram@ens-lyon.org>  Wed, 11 Aug 2004 15:26:05 +0200

ocaml-doc (3.07-1) unstable; urgency=low

  * New upstream release

 -- Remi Vanicat <vanicat@debian.org>  Wed,  8 Oct 2003 15:41:57 +0200

ocaml-doc (3.06.1-2) unstable; urgency=low

  * change some links in the camlp4.html documentation (closes: #165189)

 -- Remi Vanicat <vanicat@debian.org>  Thu, 17 Oct 2002 16:43:19 +0200

ocaml-doc (3.06.1-1) unstable; urgency=low

  * New Enhanced Ocaml Documentation
  * Some minor change to control

 -- Remi Vanicat <vanicat@debian.org>  Mon,  9 Sep 2002 16:31:03 +0200

ocaml-doc (3.06-1) unstable; urgency=low

  * New upstream (closes: #137776)
  * corrected the examples

 -- Remi Vanicat <vanicat@debian.org>  Tue, 20 Aug 2002 18:41:56 +0200

ocaml-doc (3.04-4) unstable; urgency=low

  * New debian maintainer
  * Split doc-base file
  * Move to non-free
  * Change the copyright file to the copyright of the documentation
  * remove FAQs (their license prohibit their redistribution)
  * corrected the examples

 -- Vanicat Rémi <vanicat@labri.fr>  Tue,  5 Feb 2002 10:51:43 +0100

ocaml-doc (3.04-3) unstable; urgency=low

  * Fixed bad doc-base entry for examples.html. (Closes:#132410)

 -- Sven Luther <luther@debian.org>  Tue,  5 Feb 2002 10:51:43 +0100

ocaml-doc (3.04-2) unstable; urgency=low

  * Moved documentation to /usr/share/doc/ocaml
  * Added camlp4 documentation. (Closes:#128405)
  * Added a symlink from /usr/share/doc/ocaml-doc/docs to
    /usr/share/doc/ocaml/docs.
  * Added ocaml example programs.
  * Added ocaml faq.

 -- Sven Luther <luther@debian.org>  Mon,  4 Feb 2002 11:07:04 +0100

ocaml-doc (3.04-1) unstable; urgency=low

  * New upstream release of ocaml.

 -- Sven Luther <luther@debian.org>  Thu, 20 Dec 2001 09:56:26 +0100

ocaml-doc (3.02-1) unstable; urgency=low

  * New upstream release of ocaml.

 -- Sven Luther <luther@debian.org>  Mon, 30 Jul 2001 19:04:20 +0200

ocaml-doc (3.01a-2) unstable; urgency=low

  * Changed the doc-base stuff to say 3.01 instead of 3.00.

 -- Sven Luther <luther@debian.org>  Wed, 18 Apr 2001 17:29:54 +0200

ocaml-doc (3.01a-1) unstable; urgency=low

  * Added the enhanced documentation from Hendrik Tews.

 -- Sven Luther <luther@debian.org>  Thu, 15 Mar 2001 11:27:03 +0100

ocaml-doc (3.01-1) unstable; urgency=low

  * New upstream release.

 -- Sven Luther <luther@debian.org>  Thu, 15 Mar 2001 10:27:03 +0100

ocaml-doc (3.00-2) unstable; urgency=low

  * Added doc-base stuff. CLOSES:BUG#66768
  * Upgraded latest Standards-Version.

 -- Sven Luther <luther@debian.org>  Tue, 16 Jan 2001 14:20:52 +0100

ocaml-doc (3.00-1) unstable; urgency=low

  * New upstream release.

 -- Sven Luther <luther@debian.org>  Thu, 27 Apr 2000 17:06:22 +0200

ocaml3-doc (2.99-1) unstable; urgency=low

  * New upstream release. This is the doc for the beta version of ocaml.

 -- Sven Luther <luther@debian.org>  Mon,  6 Mar 2000 13:58:31 +0100

ocaml-doc (2.04-2) frozen unstable; urgency=low

  * frozen upload.
  * fixed some lintian warnings.

 -- Sven Luther <luther@debian.org>  Fri,  4 Feb 2000 16:54:36 +0100

ocaml-doc (2.04-1) unstable; urgency=low

  * Fix bug #53521.
  * New upstream release.

 -- Sven Luther <luther@debian.org>  Wed, 26 Jan 2000 15:14:04 +0100

ocaml-doc (2.03-1) unstable; urgency=low

  * New upstream release.
  * ocaml license change, can go in main now.

 -- Sven Luther <luther@debian.org>  Mon, 22 Nov 1999 16:44:03 +0100

ocaml-doc (2.02-2) unstable; urgency=low

  * Changed architecture from any to all, as should be.

 -- Sven Luther <luther@debian.org>  Mon, 18 Oct 1999 18:24:45 +0200

ocaml-doc (2.02-1) unstable; urgency=low

  * Initial Release.

 -- Sven Luther <luther@debian.org>  Sat,  9 Oct 1999 13:24:05 +0200
